// Push the button to turn on the LED
use std::{thread, time};

use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use esp_idf_hal::gpio::PinDriver;
use esp_idf_hal::prelude::*;

use esp_idf_svc::log::EspLogger;

fn main() {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    EspLogger::initialize_default();

    let peripherals = Peripherals::take().unwrap();

    let mut led = PinDriver::output(peripherals.pins.gpio10).unwrap();
    let button = PinDriver::input(peripherals.pins.gpio8).unwrap();

    led.set_high().unwrap();
    thread::sleep(time::Duration::from_millis(1000));
    led.set_low().unwrap();

    loop {
        if button.is_high() {
            led.set_low().unwrap();
        } else {
            led.set_high().unwrap();
        }
        thread::sleep(time::Duration::from_millis(10)); // So the background task does not starve
    }
}
