use std::{thread, time};

use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use esp_idf_hal::adc;
use esp_idf_hal::gpio;
use esp_idf_hal::prelude::*;

use esp_idf_svc::log::EspLogger;

fn main() {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    EspLogger::initialize_default();

    let peripherals = Peripherals::take().unwrap();

    let mut sensors_power_enable = gpio::PinDriver::output(peripherals.pins.gpio7).unwrap();
    sensors_power_enable.set_high().unwrap();

    let adc_config = adc::AdcConfig::new().calibration(true);
    let mut adc = adc::AdcDriver::new(peripherals.adc1, &adc_config).unwrap();
    let mut adc_pin: adc::AdcChannelDriver<'_, gpio::Gpio4, adc::Atten11dB<_>> =
        adc::AdcChannelDriver::new(peripherals.pins.gpio4).unwrap();

    loop {
        let raw: u16 = adc.read(&mut adc_pin).unwrap();
        let batt: f32 = 4.2 * raw as f32 / (4096.0 / 2.0);
        println!("Battery voltage: {batt} V");

        thread::sleep(time::Duration::from_millis(300));
    }
}
