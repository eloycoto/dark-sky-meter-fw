use crate::{app_configuration::CURRENT_CONFIG, device_info::get_device_id};
use embedded_svc::mqtt;
use esp_idf_svc::mqtt::client::{EspMqttClient, MqttClientConfiguration};
use esp_idf_sys::EspError;
use log::*;
use std::sync::Mutex;

const TOPIC_BASE: &str = "dark-sky";

static PUBLISH_QUEUE_LEN: Mutex<u32> = Mutex::new(0);

pub struct MqttManager {
    topic_up: String,
    //    topic_down: String,
    client: EspMqttClient,
}

impl MqttManager {
    pub fn new() -> Result<Self, EspError> {
        let app_config = CURRENT_CONFIG.lock().unwrap();

        let broker_url = format!("mqtt://{}:{}", app_config.mqtt_server, app_config.mqtt_port);

        let use_user_pass = !app_config.mqtt_user.is_empty() & !app_config.mqtt_pass.is_empty();

        let mqtt_config = MqttClientConfiguration {
            username: match use_user_pass {
                false => None,
                true => Some(app_config.mqtt_user.as_str()),
            },
            password: match use_user_pass {
                false => None,
                true => Some(app_config.mqtt_pass.as_str()),
            },
            ..Default::default()
        };

        let client =
            EspMqttClient::new(
                broker_url,
                &mqtt_config,
                move |message_event| match message_event {
                    Ok(m) => {
                        if let mqtt::client::Event::Published(e) = m {
                            let mut the_len = PUBLISH_QUEUE_LEN.lock().unwrap();
                            *the_len -= 1;
                            debug!("MQTT Published {:?} (len {})", e, *the_len);
                        }
                    }
                    Err(e) => error!("MQTT error {:?}", e),
                },
            )?;

        Ok(MqttManager {
            topic_up: format!("{}/up/{}", TOPIC_BASE, get_device_id()),
            // topic_down: format!("{}/down", TOPIC_BASE),
            client,
        })
    }

    // NOTE: On publish return the message is enqueued but maybe not sent yet.
    // You should wait a number of seconds or wait for get_publish_queue_len() == 0.
    pub fn publish(&mut self, payload: &str) -> Result<mqtt::client::MessageId, EspError> {
        info!("Publishing to {:?}", self.topic_up.as_str());
        {
            let mut the_len = PUBLISH_QUEUE_LEN.lock().unwrap();
            *the_len += 1;
        }
        self.client.publish(
            self.topic_up.as_str(),
            mqtt::client::QoS::ExactlyOnce,
            true,
            payload.as_bytes(),
        )
    }

    // pub fn subscribe(&mut self) -> Result<mqtt::client::MessageId, EspError> {
    //     self.client
    //         .subscribe(self.topic_down.as_str(), mqtt::client::QoS::AtMostOnce)
    // }

    pub fn disconnect(self) {
        drop(self.client);
    }
}

pub fn get_publish_queue_len() -> u32 {
    *PUBLISH_QUEUE_LEN.lock().unwrap()
}
