use embedded_svc::{http::Method, io::Write};
use esp_idf_svc::errors::EspIOError;
use esp_idf_svc::http::server::{Configuration, EspHttpServer};
use log::*;
use std::sync::Mutex;

use crate::app_configuration::store_config_in_nvs;
use crate::app_configuration::AppConfiguration;
use crate::device_info::{
    get_device_id, get_firmware_date, get_firmware_time, get_firmware_version,
};
use crate::ota::ota_find;

use crate::app_configuration::CURRENT_CONFIG;

pub static IS_RESET_REQUESTED_FROM_WEB_SERVER: Mutex<bool> = Mutex::new(false);

const _URI_RESTARTED: &str = "/restarted";

pub fn web_server_requests_restart() -> bool {
    *IS_RESET_REQUESTED_FROM_WEB_SERVER.lock().unwrap()
}

pub fn launch_web_server() -> Result<EspHttpServer, EspIOError> {
    // Set the HTTP server
    let mut server = EspHttpServer::new(&Configuration::default())?;

    server.fn_handler("/", Method::Get, |request| {
        debug!("web server: / called");
        let html = index_html();
        let mut response = request.into_ok_response()?;
        response.write_all(html.as_bytes())?;
        Ok(())
    })?;

    server.fn_handler("/restart", Method::Get, |request| {
        debug!("web server: /restart called");
        let html = reset_html();
        let mut response = request.into_ok_response()?;
        response.write_all(html.as_bytes())?;
        {
            let mut must_reset = IS_RESET_REQUESTED_FROM_WEB_SERVER.lock().unwrap();
            *must_reset = true;
        }
        Ok(())
    })?;

    server.fn_handler(_URI_RESTARTED, Method::Get, |request| {
        debug!("web server: {:?} called", _URI_RESTARTED);
        let html = after_restart_html();
        let mut response = request.into_ok_response()?;
        response.write_all(html.as_bytes())?;
        Ok(())
    })?;

    server.fn_handler("/ota", Method::Get, |request| {
        debug!("web server: GET /ota called");
        let html = ota_form_html();
        let mut response = request.into_ok_response()?;
        response.write_all(html.as_bytes())?;
        Ok(())
    })?;

    server.fn_handler(
        "/ota",
        Method::Post,
        move |mut request: embedded_svc::http::server::Request<
            &mut esp_idf_svc::http::server::EspHttpConnection,
        >| {
            debug!("web server: POST /ota called");
            let mut buf: [u8; 256] = [0; 256];

            let bytes_read = request.read(&mut buf).unwrap();
            // TODO: Deal with a large (>256) number of bytes from the form. Currently only reads the first 256 bytes
            if bytes_read == buf.len() {
                let mut response = request.into_response(400, Some("Too big response"), &[])?;
                response.write_all(b"Too big response")?;
                return Ok(());
            }
            let ota_response_raw = std::str::from_utf8(&buf[0..bytes_read]).unwrap();
            debug!(
                "OTA form response {:?} -{:?}-",
                bytes_read, ota_response_raw
            );

            let firmware_url = ota_response_raw
                .split('&')
                .find(|v| v.starts_with("url="))
                .map(|v| v.get(4..).take())
                .unwrap();

            let html = match firmware_url.is_none() {
                true => "Couldn't find the firmware file. Please, check your URL.",
                false => {
                    match ota_find(
                        &firmware_url
                            .unwrap()
                            .replace("%3A", ":")
                            .replace("%2F", "/"),
                    ) {
                        Ok(_) => "OTA performed OK. Reset the device",
                        Err(e) => {
                            warn!("Couldn't perform OTA ({:?})", e);
                            "Couldn't perform OTA with given firmware"
                        }
                    }
                }
            };

            let mut response = request.into_ok_response()?;
            response.write_all(html.as_bytes())?;
            Ok(())
        },
    )?;

    server.fn_handler("/config", Method::Get, move |request| {
        debug!("web server: GET /config called");
        let html = config_form_html();
        let mut response = request.into_ok_response()?;
        response.write_all(html.as_bytes())?;
        Ok(())
    })?;

    server.fn_handler(
        "/config",
        Method::Post,
        move |mut request: embedded_svc::http::server::Request<
            &mut esp_idf_svc::http::server::EspHttpConnection,
        >| {
            debug!("web server: POST /config called");
            let mut buf: [u8; 256] = [0; 256];
            let bytes_read = request.read(&mut buf).unwrap();
            // TODO: Deal with a large (>256) number of bytes from the form. Currently only reads the first 256 bytes
            if bytes_read == buf.len() {
                let mut response = request.into_response(400, Some("Too big response"), &[])?;
                response.write_all(b"Too big response")?;
                return Ok(());
            }
            let config_raw = std::str::from_utf8(&buf[0..bytes_read]).unwrap();
            // Do not print this or you'll leak the passwords!
            //info!("config data {:?} -{:?}-", bytes_read, config_raw);

            let mut app_config = CURRENT_CONFIG.lock().unwrap();

            for token in config_raw.rsplit('&') {
                update_config_with_token(&mut app_config, token);
            }

            store_config_in_nvs(app_config);

            let html = config_form_html();
            let mut response = request.into_ok_response()?;
            response.write_all(html.as_bytes())?;
            Ok(())
        },
    )?;

    Ok(server)
}

fn templated(content: impl AsRef<str>) -> String {
    format!(
        r#"
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Dark Sky meter web server</title>
    </head>
    <body>
        {}
        <br>
        <hr>
        <b>Dark Sky meter</b><br>
        Device ID: {}.<br>Firmware version {}.<br>Compilation date-time: {}-{}.
    </body>
</html>
"#,
        content.as_ref(),
        get_device_id(),
        get_firmware_version(),
        get_firmware_date(),
        get_firmware_time(),
    )
}

fn reset_html() -> String {
    // When the logger is sending this HTML, other thread is waiting to reset the board,
    // so redirect_millis should be less that what takes the board to start the reset.
    let redirect_millis: u32 = 100;

    templated(format!(
        r#"
    <p id="result"></p>
    <script>
     function myURL() {{
         document.location.href = '{_URI_RESTARTED}';
         clearInterval(interval);
      }}
     var interval = setInterval(myURL, {redirect_millis});
     var result = document.getElementById("result");
     result.innerHTML = "The board is being reset...";
   </script>
    "#
    ))
}

fn after_restart_html() -> String {
    templated(
        r#"
    <h1>Board reset complete</h1></br>
    The board should be logging now and this web is no longer active.</br></br>
    To run again this configuration web, please maintain the USER button pressed while the board is restarting.
    "#,
    )
}

fn index_html() -> String {
    templated(
        r#"
<h1>Hello from Dark Sky meter!</h1>
Go to the <a href="/config">configuration form</a>.</br>
Go to the <a href="/ota">OTA page</a>.
"#,
    )
}

fn ota_form_html() -> String {
    templated(
        r#"
<form action="/ota" method="post">
<H2>Firmware URL</H2><br>
    URL: <input type="text" maxlength="200" name="url" value="">
<br>
<input type="submit" value="Submit">
    <button onclick="location.href='/restart'" type="button">Restart board</button>
</form>
Go to the <a href="/">main page</a>.
"#,
    )
}

fn config_form_html() -> String {
    let app_config = CURRENT_CONFIG.lock().unwrap();

    templated(format!(
        "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
        r#"
<form action="/config" method="post">
<H2>Light sensor</H2>
    Integration time:
    <select name="int_time">
        <option value="_100MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_100MS")
        {
            "selected"
        } else {
            ""
        },
        r#">100 milliseconds</option>
        <option value="_200MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_200MS")
        {
            "selected"
        } else {
            ""
        },
        r#">200 milliseconds</option>
        <option value="_300MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_300MS")
        {
            "selected"
        } else {
            ""
        },
        r#">300 milliseconds</option>
        <option value="_400MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_400MS")
        {
            "selected"
        } else {
            ""
        },
        r#">400 milliseconds</option>
        <option value="_500MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_500MS")
        {
            "selected"
        } else {
            ""
        },
        r#">500 milliseconds</option>
        <option value="_600MS" "#,
        if app_config
            .light_sensor_integration_time
            .starts_with("_600MS")
        {
            "selected"
        } else {
            ""
        },
        r#">600 milliseconds</option>
    </select>
<br>
    Gain:
    <select name="gain">
        <option value="LOW" "#,
        if app_config.light_sensor_gain.starts_with("LOW") {
            "selected"
        } else {
            ""
        },
        r#">low gain (1x)</option>
        <option value="MED" "#,
        if app_config.light_sensor_gain.starts_with("MED") {
            "selected"
        } else {
            ""
        },
        r#">medium gain (25x)</option>
        <option value="HIGH" "#,
        if app_config.light_sensor_gain.starts_with("HIGH") {
            "selected"
        } else {
            ""
        },
        r#">medium gain (428x)</option>
        <option value="MAX" "#,
        if app_config.light_sensor_gain.starts_with("MAX") {
            "selected"
        } else {
            ""
        },
        r#">max gain (9876x)</option>
    </select>
<br>

<H2>External WiFi</H2><br>
    SSID: <input type="text" maxlength="20" name="external_wifi_ssid" value=""#,
        app_config.external_wifi_ssid,
        r#"">
<br>
    Password: <input type="password" maxlength="20" name="external_wifi_pass" value=""#,
        app_config.external_wifi_psk,
        r#"">
<br>
<H2>Captive WiFi</H2>
    SSID: <input type="text" maxlength="20" name="captive_wifi_ssid" value=""#,
        app_config.captive_wifi_ssid,
        r#"">
<br>
    Password: <input type="password" maxlength="20" name="captive_wifi_pass" value=""#,
        app_config.captive_wifi_psk,
        r#"">
<br>
<H2>MQTT</H2>
    Server: <input type="text" maxlength="20" name="mqtt_server" value=""#,
        app_config.mqtt_server,
        r#"">
<br>
    Port: <input type="number" min="1" max="65535" name="mqtt_port" value=""#,
        app_config.mqtt_port,
        r#"">
<br>
User (empty for no user): <input type="text" maxlength="20" name="mqtt_user" value=""#,
        app_config.mqtt_user,
        r#"">
Password: <input type="password" maxlength="20" name="mqtt_pass" value=""#,
        app_config.mqtt_pass,
        r#"">
<br><br><br>
    <input type="submit" value="Submit">
    <button onclick="location.href='/restart'" type="button">Restart board</button>
</form>
Go to the <a href="/">main page</a>.
"#
    ))
}

fn update_config_with_token(config: &mut AppConfiguration, token: &str) {
    if token.starts_with("external_wifi_ssid") {
        config.external_wifi_ssid = token.split('=').last().unwrap_or("").to_string();
        debug!(
            "external_wifi_ssid updated to {:?}",
            config.external_wifi_ssid
        );
        return;
    };

    if token.starts_with("external_wifi_pass") {
        config.external_wifi_psk = token.split('=').last().unwrap_or("").to_string();
        debug!(
            "external_wifi_pass updated to {:?}",
            "REDACTED" /* config.external_wifi_psk */
        );
        return;
    };

    if token.starts_with("captive_wifi_ssid") {
        config.captive_wifi_ssid = token.split('=').last().unwrap_or("").to_string();
        debug!(
            "captive_wifi_ssid updated to {:?}",
            config.captive_wifi_ssid
        );
        return;
    };

    if token.starts_with("captive_wifi_pass") {
        config.captive_wifi_psk = token.split('=').last().unwrap_or("").to_string();
        debug!(
            "captive_wifi_pass updated to {:?}",
            "REDACTED" /* config.captive_wifi_psk*/
        );
        return;
    };

    if token.starts_with("mqtt_server") {
        config.mqtt_server = token.split('=').last().unwrap_or("").to_string();
        debug!("mqtt_server updated to {:?}", config.mqtt_server);
        return;
    };

    if token.starts_with("mqtt_port") {
        let v = token.split('=').last().unwrap_or("1883");
        config.mqtt_port = v.parse::<u16>().unwrap_or(1883);
        debug!("mqtt_port updated to {:?}", config.mqtt_port);
        return;
    };

    if token.starts_with("mqtt_user") {
        config.mqtt_user = token.split('=').last().unwrap_or("").to_string();
        debug!("mqtt_user updated to {:?}", config.mqtt_user);
        return;
    };

    if token.starts_with("mqtt_pass") {
        config.mqtt_pass = token.split('=').last().unwrap_or("").to_string();
        debug!(
            "mqtt_pass updated to {:?}",
            "REDACTED" /* config.mqtt_pass */
        );
        return;
    };

    if token.starts_with("int_time") {
        config.light_sensor_integration_time =
            token.split('=').last().unwrap_or("_200MS").to_string();
        debug!(
            "light sensor integration time updated to {:?}",
            config.light_sensor_integration_time
        );
        return;
    };

    if token.starts_with("gain") {
        config.light_sensor_gain = token.split('=').last().unwrap_or("MED").to_string();
        debug!(
            "light sensor gain updated to {:?}",
            config.light_sensor_gain
        );
        return;
    };
}
