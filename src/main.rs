use esp_idf_sys::{self as _}; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use esp_idf_hal::adc::{AdcChannelDriver, AdcConfig, AdcDriver};
use esp_idf_hal::gpio;
use esp_idf_hal::i2c::{I2cConfig, I2cDriver};
use esp_idf_hal::prelude::*;
use esp_idf_hal::reset::restart;
use esp_idf_svc::eventloop::EspSystemEventLoop;
use esp_idf_svc::log::{set_target_level, EspLogger};

use log::*;

use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::{thread, time};

mod app_configuration;
use crate::app_configuration::{
    print_current_censored_config, update_config_from_nvs, CURRENT_CONFIG,
};
mod mqtt;
mod network_manager;
mod ota;
mod sensors;
mod web_server;
use crate::web_server::web_server_requests_restart;
mod device_info;
use crate::device_info::{
    get_device_id, get_firmware_date, get_firmware_time, get_firmware_version,
};

type LedPin<'d> = gpio::PinDriver<'d, gpio::Gpio10, gpio::Output>;

fn led_blink(led: &mut LedPin, n: u8) {
    for _ in 1..(n * 2) {
        led.toggle().unwrap();
        thread::sleep(time::Duration::from_millis(200));
    }
}

fn main() {
    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. See https://github.com/esp-rs/esp-idf-template/issues/71
    esp_idf_sys::link_patches();

    let start_time = time::SystemTime::now();

    EspLogger::initialize_default();
    // set_target_level("rust-logging", LevelFilter::Trace); // This can be set to LevelFilter::Off for no messages (except from the bootloader)
    set_target_level("*", LevelFilter::Trace); // This can be set to LevelFilter::Off for no messages (except from the bootloader)

    info!(
        "Starting Dark Sky meter for device ID {:?}. Firmware version {:?}. Compiled at {:?} {:?}",
        get_device_id(),
        get_firmware_version(),
        get_firmware_date(),
        get_firmware_time()
    );

    let wakeup_reason = esp_idf_hal::reset::WakeupReason::get();
    info!("Wakeup reason: {:?}", wakeup_reason);
    let reset_reason = esp_idf_hal::reset::ResetReason::get();
    info!("Reset reason: {:?}", reset_reason);
    info!("Start time: {:?}", start_time);

    let peripherals = Peripherals::take().unwrap();
    let mut led: LedPin = gpio::PinDriver::output(peripherals.pins.gpio10).unwrap();
    let button = gpio::PinDriver::input(peripherals.pins.gpio8).unwrap();

    let is_cfg_mode = if button.is_low() {
        led.set_high().unwrap();
        true
    } else {
        led.set_low().unwrap();
        false
    };

    let sysloop = EspSystemEventLoop::take().unwrap();

    update_config_from_nvs();
    print_current_censored_config();

    let mut networking =
        match network_manager::NetworkManager::get_network_manager(peripherals.modem, sysloop) {
            Ok(working_network) => working_network,
            Err(not_working_network) => match not_working_network {
                network_manager::NetworkError::MissingAttribute => {
                    panic!()
                }
                network_manager::NetworkError::EspDerivedError { source } => {
                    panic!("ESP lib related error setting up WiFi: {}", source)
                }
                network_manager::NetworkError::SNTPTimeout => {
                    panic!()
                }
                network_manager::NetworkError::WiFiTimeout => {
                    panic!()
                }
            },
        };

    if is_cfg_mode {
        info!("Configuration mode");
        match networking.connect_to_wifi(true) {
            Ok(_) => {}
            Err(e) => warn!("Couldn't connect to WiFi {:?}", e),
        };

        let _web_server = match web_server::launch_web_server() {
            Ok(server) => {
                trace!("Web server OK!");
                server
            }
            Err(_) => {
                panic!("Web server ERROR!");
            }
        };

        loop {
            debug!("Reset device to start sampling");
            thread::sleep(time::Duration::from_millis(1000));
            if web_server_requests_restart() {
                warn!("Restarting board as requested by the user");
                thread::sleep(time::Duration::from_millis(1250)); // So the server can return a page
                restart();
            }
        } // If in configuration mode never leaves this loop.
    }

    let data = Arc::new(Mutex::new(sensors::SensorsData::new()));
    let data_ref = Arc::clone(&data);
    let sampling_thread = thread::spawn(move || {
        // Sensors excitation
        // thread::sleep(time::Duration::from_millis(100));

        let mut sensors_power_enable = gpio::PinDriver::output(peripherals.pins.gpio7).unwrap();
        sensors_power_enable.set_high().unwrap();

        let adc_config = AdcConfig::new().calibration(true);
        let mut adc = AdcDriver::new(peripherals.adc1, &adc_config).unwrap();
        let mut bat_adc: AdcChannelDriver<'_, gpio::Gpio4, esp_idf_hal::adc::Atten11dB<_>> =
            AdcChannelDriver::new(peripherals.pins.gpio4).unwrap();
        let bat_raw = adc.read(&mut bat_adc).unwrap();
        let batt: f32 = 4.2 * bat_raw as f32 / (4096.0 / 2.0);

        let i2c_config = I2cConfig::new().baudrate(50.kHz().into());
        let sda = peripherals.pins.gpio5;
        let scl = peripherals.pins.gpio6;
        let i2c = I2cDriver::new(peripherals.i2c0, sda, scl, &i2c_config).unwrap();

        sensors::sampling_task(i2c, batt, data_ref).unwrap();

        sensors_power_enable.set_low().unwrap();
    });

    match networking.connect_to_wifi(false) {
        Ok(_) => {}
        Err(e) => panic!("Couldn't connect to WiFi {:?}", e),
    };

    match network_manager::sync_ntp() {
        Ok(_) => {
            trace!("NTP succeeded!");
        }
        Err(_) => {
            warn!("NTP failed!");
        }
    };

    // Connect to the MQTT broker
    let mut mqtt = match mqtt::MqttManager::new() {
        Ok(inner) => inner,
        Err(err) => panic!("MQTT KO: {err}"),
    };

    // match mqtt.subscribe() {
    //     Ok(_) => {
    //         println!("MQTT sub OK!");
    //     }
    //     Err(_) => {
    //         println!("MQTT sub ERROR!");
    //     }
    // };

    sampling_thread.join().unwrap();
    let data = data.lock().unwrap();
    let data_str = serde_json::to_string(&(*data)).unwrap();
    match mqtt.publish(data_str.as_str()) {
        Ok(_) => {
            trace!("MQTT pub OK!");
            debug!("Payload: {data_str}");
        }
        Err(_) => {
            warn!("MQTT pub ERROR!");
        }
    };

    for _ in 1..10 {
        if mqtt::get_publish_queue_len() == 0 {
            break;
        }
        debug!("MQTT publish queue len: {}", mqtt::get_publish_queue_len());
        thread::sleep(time::Duration::from_secs(1));
    }

    mqtt.disconnect();
    networking.disconnect().unwrap();

    let end_blinks: u8 = 3;

    let sleep_micros: u64 = {
        let blink_duration = Duration::new(0, 400_000_000 * (end_blinks as u32));
        let restart_duration = Duration::new(0, 600_000_000);

        let max_sleep_time = {
            Duration::new(
                CURRENT_CONFIG.lock().unwrap().seconds_between_wake_up as u64,
                0,
            )
        };

        let loop_seconds = time::SystemTime::now()
            .duration_since(start_time)
            .unwrap_or(Duration::new(0, 0));

        let sleep_duration = max_sleep_time
            .saturating_sub(loop_seconds)
            .saturating_sub(blink_duration)
            .saturating_sub(restart_duration);

        match sleep_duration.is_zero() {
            true => 1, // sleep something
            false => sleep_duration.as_micros() as u64,
        }
    };

    unsafe {
        esp_idf_sys::esp_sleep_enable_timer_wakeup(sleep_micros);

        info!("Going to deep sleep {} seconds", sleep_micros / 1_000_000);
        led_blink(&mut led, end_blinks);
        esp_idf_sys::esp_deep_sleep_start();
        // Software reset!
    }
}
