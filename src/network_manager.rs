use embedded_svc::wifi::{
    AccessPointConfiguration, AuthMethod, ClientConfiguration, Configuration,
};

use esp_idf_hal::peripheral::Peripheral;
use esp_idf_svc::eventloop::EspSystemEventLoop;
use esp_idf_svc::sntp::{EspSntp, SntpConf, SyncStatus};
use esp_idf_svc::wifi::{BlockingWifi, EspWifi};
use esp_idf_sys::EspError;
use log::*;
use thiserror::Error;

use crate::app_configuration::CURRENT_CONFIG;

pub struct NetworkManager<'a> {
    wifi_handler: BlockingWifi<EspWifi<'a>>,
}

#[derive(Debug, Error)]
pub enum NetworkError {
    #[error("Missing network configuration")]
    MissingAttribute,
    #[error("SNTP Timeout")]
    SNTPTimeout,
    #[error("WiFi Timeout")]
    WiFiTimeout,
    #[error("ESP related error")]
    EspDerivedError {
        #[from]
        source: EspError,
    },
}

impl NetworkManager<'_> {
    /// Gets a new NetworkManager
    pub fn get_network_manager(
        modem: impl Peripheral<P = esp_idf_hal::modem::Modem> + 'static,
        sysloop: EspSystemEventLoop,
    ) -> Result<NetworkManager<'static>, NetworkError> {
        let wifi_handler = BlockingWifi::wrap(
            EspWifi::new(
                modem,
                sysloop.clone(),
                None, /*Some(nvs_default_partition)*/
            )?,
            sysloop,
        )?;
        let network_data = NetworkManager { wifi_handler };

        Ok(network_data)
    }

    /// Makes a connection to a given WiFi AP and set up the softAP (if asked to)
    ///
    /// IDF reference: <https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/api-reference/network/esp_wifi.html>
    pub fn connect_to_wifi(&mut self, start_soft_ap: bool) -> Result<(), NetworkError> {
        let mut auth_method = AuthMethod::WPA2Personal;
        let mut ap_auth_method = AuthMethod::WPA2Personal;

        let app_config = CURRENT_CONFIG.lock().unwrap();

        if app_config.external_wifi_ssid.is_empty() {
            return Err(NetworkError::MissingAttribute);
        }

        if app_config.external_wifi_psk.is_empty() {
            auth_method = AuthMethod::None;
        }

        if start_soft_ap {
            if app_config.captive_wifi_ssid.is_empty() {
                return Err(NetworkError::MissingAttribute);
            }

            if app_config.captive_wifi_psk.is_empty() {
                ap_auth_method = AuthMethod::None;
            }
        }

        let conf = match start_soft_ap {
            true => {
                info!("Starting WiFi with SoftAP");
                Configuration::Mixed(
                    ClientConfiguration {
                        ssid: app_config.external_wifi_ssid.as_str().into(),
                        password: app_config.external_wifi_psk.as_str().into(),
                        auth_method,
                        ..Default::default()
                    },
                    AccessPointConfiguration {
                        ssid: app_config.captive_wifi_ssid.as_str().into(),
                        password: app_config.captive_wifi_psk.as_str().into(),
                        auth_method: ap_auth_method,
                        ..Default::default()
                    },
                )
            }
            false => {
                info!("Starting WiFi without SoftAP");
                Configuration::Client(ClientConfiguration {
                    ssid: app_config.external_wifi_ssid.as_str().into(),
                    password: app_config.external_wifi_psk.as_str().into(),
                    auth_method,
                    ..Default::default()
                })
            }
        };

        self.wifi_handler.set_configuration(&conf)?;

        info!(
            "WiFi configuration: {:?}",
            self.wifi_handler.get_configuration().unwrap()
        );

        debug!("Wifi starting...");
        self.wifi_handler.start()?;

        debug!("Wifi connecting...");
        self.wifi_handler.connect()?;

        debug!("Wifi netif bring up...");
        self.wifi_handler.wait_netif_up()?;

        let ip_info = self.wifi_handler.wifi().sta_netif().get_ip_info()?;
        info!("Wifi DHCP info: {:?}", ip_info);

        Ok(())
    }

    pub fn disconnect(&mut self) -> Result<(), NetworkError> {
        let t0 = std::time::SystemTime::now();
        self.wifi_handler.disconnect()?;

        while self.wifi_handler.is_connected().unwrap_or(false) {
            if std::time::SystemTime::now() > t0 + std::time::Duration::from_secs(20) {
                warn!("Disconnection error");
                return Err(NetworkError::WiFiTimeout);
            }
        }

        Ok(())
    }
}

pub fn sync_ntp() -> Result<(), NetworkError> {
    debug!("Connecting SNTP...");
    let sntp_conf = SntpConf::default();
    let sntp = EspSntp::new(&sntp_conf)?;

    let t0 = std::time::SystemTime::now();
    while sntp.get_sync_status() != SyncStatus::Completed {
        std::thread::sleep(std::time::Duration::from_millis(500));

        if std::time::SystemTime::now() > t0 + std::time::Duration::from_secs(20) {
            warn!("SNTP sync timed out");
            return Err(NetworkError::SNTPTimeout);
        }
    }

    Ok(())
}
