use esp_idf_hal::delay;
use log::*;
use serde::Serialize;
use std::sync::{Arc, Mutex};
use std::time;
use thiserror::Error;
use tsl2591_eh_driver::{Gain, IntegrationTimes};

use crate::CURRENT_CONFIG;

// The data produced by the sensors after processing.
// Serializable, so we can send it easily.
#[derive(Serialize, Debug)]
pub struct SensorsData {
    pub ts: u32,
    pub bat: f32,
    pub accel: Option<(f32, f32, f32)>, // Mean value of the taken samples
    pub lux: Option<(u16, u16, f32)>,   // Mean value of the taken samples
    pub amb: Option<(f32, f32, f32)>,   // Mean value of the taken samples
    pub accel_sd: Option<(f32, f32, f32)>, // Standard deviation of the taken samples
    pub lux_sd: Option<(f32, f32, f32)>, // Standard deviation of the taken samples
    pub amb_sd: Option<(f32, f32, f32)>, // Standard deviation of the taken samples
}

impl SensorsData {
    pub fn new() -> Self {
        SensorsData {
            ts: 0,
            bat: 0f32,
            accel: None,
            lux: None,
            amb: None,
            accel_sd: None,
            lux_sd: None,
            amb_sd: None,
        }
    }

    pub fn update_timestamp(&mut self) {
        self.ts = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH)
            .unwrap_or(time::Duration::from_secs(0))
            .as_secs() as u32;
    }
}

#[derive(Debug, Error)]
pub enum SensorError {
    #[error("Sensor not responding")]
    NotResponding,
    #[error("Sensor value overflow")]
    Overflow,
}

pub struct AccelSensor<F> {
    driver: adxl345_eh_driver::Driver<F>,
}

impl<F> AccelSensor<F>
where
    F: embedded_hal::i2c::I2c,
{
    pub fn new(i2c: F) -> Result<Self, SensorError> {
        let driver = match adxl345_eh_driver::Driver::new(
            i2c,
            Some(adxl345_eh_driver::address::SECONDARY),
        ) {
            Ok(handler) => handler,
            Err(_) => {
                warn!("ADXL345 not responding");
                return Err(SensorError::NotResponding);
            }
        };

        Ok(Self { driver })
    }

    pub fn get_data(&mut self) -> Result<(f32, f32, f32), SensorError> {
        let data = self.driver.get_accel().unwrap();
        info!("Accel: {:?}", data);

        Ok(data)
    }
}

pub struct LightSensor<F> {
    driver: tsl2591_eh_driver::Driver<F>,
}

impl<F> LightSensor<F>
where
    F: embedded_hal::i2c::I2c,
{
    pub fn new(
        i2c: F,
        integration_time: IntegrationTimes,
        gain: Gain,
    ) -> Result<Self, SensorError> {
        let mut driver =
            match tsl2591_eh_driver::Driver::new_define_integration(i2c, integration_time, gain) {
                Ok(handler) => handler,
                Err(_) => {
                    warn!("TSL2591 not responding");
                    return Err(SensorError::NotResponding);
                }
            };
        driver.enable().unwrap();

        Ok(Self { driver })
    }

    pub fn get_data(&mut self) -> Result<(u16, u16, f32), SensorError> {
        let (ch_0, ch_1) = self.driver.get_channel_data(&mut delay::FreeRtos).unwrap();
        let lux = match self.driver.calculate_lux(ch_0, ch_1) {
            Ok(v) => v,
            Err(e) => {
                error!("Light sensor: {:?}", e);
                match e {
                    tsl2591_eh_driver::Error::SignalOverflow() => {
                        return Err(SensorError::Overflow)
                    }
                    _ => return Err(SensorError::NotResponding),
                }
            }
        };
        info!("Light: {:?}:{:?}", (ch_0, ch_1), lux);

        Ok((ch_0, ch_1, lux))
    }
}

pub struct EnvSensor<F> {
    driver: bme280::i2c::BME280<F>,
}

impl<F> EnvSensor<F>
where
    F: embedded_hal::i2c::I2c,
{
    pub fn new(i2c: F) -> Result<Self, SensorError> {
        let mut driver = bme280::i2c::BME280::new_secondary(i2c);
        match driver.init(&mut delay::FreeRtos) {
            Ok(handler) => handler,
            Err(_) => {
                warn!("BME280 not responding");
                return Err(SensorError::NotResponding);
            }
        };

        Ok(Self { driver })
    }

    pub fn get_data(&mut self) -> Result<(f32, f32, f32), SensorError> {
        let m = self.driver.measure(&mut delay::FreeRtos).unwrap();
        let data = (m.temperature, m.humidity, m.pressure);
        info!("Ambient: {:?}", data);

        Ok(data)
    }
}

fn to_integration_times(val: &String) -> IntegrationTimes {
    if val.starts_with("_100MS") {
        return IntegrationTimes::_100MS;
    }

    if val.starts_with("_200MS") {
        return IntegrationTimes::_200MS;
    }

    if val.starts_with("_300MS") {
        return IntegrationTimes::_300MS;
    }

    if val.starts_with("_400MS") {
        return IntegrationTimes::_400MS;
    }

    if val.starts_with("_500MS") {
        return IntegrationTimes::_500MS;
    }

    if val.starts_with("_600MS") {
        return IntegrationTimes::_600MS;
    }

    warn!(
        "Wrong light sensor integration time configuration value {:?}. Using default {:?}.",
        val, "_200MS"
    );
    IntegrationTimes::_200MS
}

fn to_gain(val: &String) -> Gain {
    if val.starts_with("LOW") {
        return Gain::LOW;
    }

    if val.starts_with("MED") {
        return Gain::MED;
    }

    if val.starts_with("HIGH") {
        return Gain::HIGH;
    }

    if val.starts_with("MAX") {
        return Gain::MAX;
    }

    warn!(
        "Wrong light sensor gain configuration value {:?}. Using default {:?}.",
        val, "MED"
    );
    Gain::MED
}

fn is_valid_light_value(v: (u16, u16, f32)) -> bool {
    const NOT_VALID_LIGHT_VALUE: (u16, u16, f32) = (0, 0, f32::NAN);

    !(v.0 == NOT_VALID_LIGHT_VALUE.0 && v.1 == NOT_VALID_LIGHT_VALUE.1 && v.2.is_nan())
}

fn calculate_mean_all_floats(v: &Vec<(f32, f32, f32)>) -> Option<(f32, f32, f32)> {
    let count = v.len() as f32;
    let v0 = v.iter().map(|i| i.0).sum::<f32>() / count;
    let v1 = v.iter().map(|i| i.1).sum::<f32>() / count;
    let v2 = v.iter().map(|i| i.2).sum::<f32>() / count;
    Some((v0, v1, v2))
}

fn calculate_mean_some_floats(v: &Vec<(u16, u16, f32)>) -> Option<(u16, u16, f32)> {
    let count = v.len() as u64;
    let v0 = (v.iter().map(|i| i.0 as u64).sum::<u64>() / count) as u16;
    let v1 = (v.iter().map(|i| i.1 as u64).sum::<u64>() / count) as u16;
    let v2 = v.iter().map(|i| i.2).sum::<f32>() / count as f32;
    Some((v0, v1, v2))
}

fn calculate_std_dev_all_floats(
    v: &Vec<(f32, f32, f32)>,
    mean: &Option<(f32, f32, f32)>,
) -> Option<(f32, f32, f32)> {
    if mean.is_none() {
        return None;
    }

    let count = v.len();
    if count <= 2 {
        return None;
    }

    let count = (count - 1) as f32;

    let v0 = (v
        .iter()
        .map(|i| {
            let diff = i.0 - mean.unwrap().0;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    let v1 = (v
        .iter()
        .map(|i| {
            let diff = i.1 - mean.unwrap().1;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    let v2 = (v
        .iter()
        .map(|i| {
            let diff = i.2 - mean.unwrap().2;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    Some((v0, v1, v2))
}

fn calculate_std_dev_some_floats(
    v: &Vec<(u16, u16, f32)>,
    mean: &Option<(u16, u16, f32)>,
) -> Option<(f32, f32, f32)> {
    if mean.is_none() {
        return None;
    }

    let count = v.len();
    if count <= 2 {
        return None;
    }

    let count = (count - 1) as f32;

    let v0 = (v
        .iter()
        .map(|i| {
            let diff = i.0 as f32 - mean.unwrap().0 as f32;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    let v1 = (v
        .iter()
        .map(|i| {
            let diff = i.1 as f32 - mean.unwrap().1 as f32;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    let v2 = (v
        .iter()
        .map(|i| {
            let diff = i.2 - mean.unwrap().2;
            diff * diff
        })
        .sum::<f32>()
        / count)
        .sqrt();

    Some((v0, v1, v2))
}

pub fn sampling_task(
    i2c: impl embedded_hal::i2c::I2c,
    bat: f32,
    data: Arc<Mutex<SensorsData>>,
) -> Result<(), SensorError> {
    const NSAMPLES: u8 = 10;

    let i2c_ref_cell = core::cell::RefCell::new(i2c);
    let mut env = match EnvSensor::new(embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref_cell)) {
        Ok(handler) => Some(handler),
        Err(_) => None,
    };
    let app_config = CURRENT_CONFIG.lock().unwrap();
    let integration_time: IntegrationTimes =
        to_integration_times(&app_config.light_sensor_integration_time);
    let gain: Gain = to_gain(&app_config.light_sensor_gain);
    let mut light = match LightSensor::new(
        embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref_cell),
        integration_time,
        gain,
    ) {
        Ok(mut handler) => {
            // discard the first value
            let _ = handler.get_data();
            Some(handler)
        }
        Err(_) => None,
    };
    let mut accel = match AccelSensor::new(embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref_cell))
    {
        Ok(handler) => Some(handler),
        Err(_) => None,
    };

    let mut amb_samples: Vec<(f32, f32, f32)> = vec![];
    let mut light_samples: Vec<(u16, u16, f32)> = vec![];
    let mut accel_samples: Vec<(f32, f32, f32)> = vec![];

    for n in 0..NSAMPLES {
        trace!("Sample number {}", n);
        if let Some(ref mut i) = env {
            amb_samples.push(i.get_data().unwrap());
        }

        if let Some(ref mut i) = light {
            let l = match i.get_data() {
                Ok(d) => d,
                Err(e) => {
                    error!("Light sensor: {:?}", e);
                    match e {
                        SensorError::NotResponding => (u16::MAX, u16::MAX, f32::NAN),
                        SensorError::Overflow => (u16::MAX, u16::MAX, f32::MAX),
                    }
                }
            };

            if is_valid_light_value(l) {
                light_samples.push(l)
            }
        }

        if let Some(ref mut i) = accel {
            accel_samples.push(i.get_data().unwrap());
        }

        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
    trace!("amb_samples {:?}", amb_samples);
    trace!("light_samples {:?}", light_samples);
    trace!("accel_samples {:?}", accel_samples);

    let mut data = data.lock().unwrap();

    data.accel = calculate_mean_all_floats(&accel_samples);
    data.accel_sd = calculate_std_dev_all_floats(&accel_samples, &data.accel);

    data.amb = calculate_mean_all_floats(&amb_samples);
    data.amb_sd = calculate_std_dev_all_floats(&amb_samples, &data.amb);

    data.lux = calculate_mean_some_floats(&light_samples);
    data.lux_sd = calculate_std_dev_some_floats(&light_samples, &data.lux);

    data.update_timestamp();
    data.bat = bat;
    info!("Battery level: {} V", bat);

    Ok(())
}
