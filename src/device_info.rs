// Functions that return device specific info (hardware and firmware related)

use std::ffi::CStr;

// Takes info from Cargo.toml and sets it to esp_app_desc_t
esp_idf_sys::esp_app_desc!();

pub fn get_firmware_version() -> String {
    unsafe { CStr::from_ptr(esp_app_desc.version.as_ptr()) }
        .to_string_lossy()
        .into_owned()
}

pub fn get_firmware_date() -> String {
    // not a bug. Time and date are flipped
    unsafe { CStr::from_ptr(esp_app_desc.time.as_ptr()) }
        .to_string_lossy()
        .into_owned()
}

pub fn get_firmware_time() -> String {
    // not a bug. Time and date are flipped
    unsafe { CStr::from_ptr(esp_app_desc.date.as_ptr()) }
        .to_string_lossy()
        .into_owned()
}

pub fn get_device_mac() -> String {
    let mut mac: [u8; 6] = [0; 6];
    unsafe {
        esp_idf_sys::esp_read_mac(mac.as_mut_ptr(), 3);
    }
    format!(
        "{:x}{:x}{:x}{:x}{:x}{:x}",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]
    )
}

pub fn get_device_id() -> String {
    get_device_mac()
}
