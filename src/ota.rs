use embedded_svc::ota::{FirmwareInfoLoader, LoadResult};
use esp_idf_svc::http::client::{Configuration, EspHttpConnection};
use esp_idf_svc::ota::{EspFirmwareInfoLoader, EspOta};
use log::*;
use thiserror::Error;

const WRITE_DATA_BUF_SIZE: usize = 1024;

#[derive(Debug, Error)]
pub enum OtaError {
    #[error("Failed to fetch OTA")]
    ServerNotAvailable,
    #[error("OTA not found")]
    FwImageNotFound,
    #[error("HTTP error")]
    HttpError,
    #[error("Failed to write OTA to flash")]
    OtaFlashError,
    #[error("OTA image not valid")]
    OtaNotValid,
    #[error("OTA information not complete")]
    OtaInfoIncomplete,
}

fn get_ota_info(loader: &mut EspFirmwareInfoLoader, data: &[u8]) -> Result<(), OtaError> {
    let res = match loader.load(data) {
        Ok(n) => n,
        Err(_) => {
            return Err(OtaError::OtaNotValid);
        }
    };

    match res {
        LoadResult::LoadMore => {
            return Err(OtaError::OtaInfoIncomplete);
        }
        LoadResult::Loaded => {
            let info = loader.get_info();
            info!("OTA info: {:?}", info);
            return Ok(());
        }
        _ => unreachable!(),
    }
}

pub fn ota_find(firmware_url: &str) -> Result<(), OtaError> {
    let content_length: usize;
    let mut ota_write_data: [u8; WRITE_DATA_BUF_SIZE] = [0; WRITE_DATA_BUF_SIZE];

    let mut client = EspHttpConnection::new(&Configuration {
        buffer_size: Some(WRITE_DATA_BUF_SIZE),
        use_global_ca_store: true,
        crt_bundle_attach: Some(esp_idf_sys::esp_crt_bundle_attach),
        ..Default::default()
    })
    .expect("creation of EspHttpConnection should have worked");

    match client.initiate_request(embedded_svc::http::Method::Get, firmware_url, &[]) {
        Ok(_) => (),
        Err(_) => return Err(OtaError::ServerNotAvailable),
    }

    match client.initiate_response() {
        Ok(_) => (),
        Err(_) => return Err(OtaError::ServerNotAvailable),
    }

    if client.status() != 200 {
        return Err(OtaError::ServerNotAvailable);
    }

    if let Some(len) = client.header("Content-Length") {
        content_length = len.parse().unwrap();
    } else {
        return Err(OtaError::FwImageNotFound);
    }

    let mut ota = EspOta::new().unwrap();
    // let boot_slot = ota.get_boot_slot().unwrap();
    // println!("{:?}", boot_slot);
    // let run_slot = ota.get_running_slot().unwrap();
    // println!("{:?}", run_slot);
    // let update_slot = ota.get_update_slot().unwrap();
    // println!("{:?}", update_slot);

    let ota_update = match ota.initiate_update() {
        Ok(handle) => handle,
        Err(_) => return Err(OtaError::OtaFlashError),
    };

    let mut data_read = 0;
    let mut loader = EspFirmwareInfoLoader::new();
    let mut fw_info_read = false;

    while data_read < content_length {
        data_read += match client.read(&mut ota_write_data) {
            Ok(n) => n,
            Err(_) => {
                ota_update.abort().unwrap();
                return Err(OtaError::HttpError);
            }
        };

        if !fw_info_read {
            match get_ota_info(&mut loader, &ota_write_data) {
                Err(OtaError::OtaInfoIncomplete) => {}
                Err(_) => {
                    ota_update.abort().unwrap();
                    return Err(OtaError::OtaFlashError);
                }
                Ok(_) => {
                    fw_info_read = true;
                }
            }
        }

        if let Err(_) = ota_update.write(&ota_write_data) {
            ota_update.abort().unwrap();
            return Err(OtaError::OtaFlashError);
        }
    }

    match ota_update.complete() {
        Ok(handle) => handle,
        Err(_) => return Err(OtaError::OtaFlashError),
    };

    Ok(())
}
